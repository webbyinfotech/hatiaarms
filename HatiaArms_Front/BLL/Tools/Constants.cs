using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Tools
{
    using System.ComponentModel;

    public class Constants
    {
        public static class Session
        {

            public const string IsAuth = "IsAuth";

            public const string ClientId = "ClientId";

            public const string CemeteryId = "CemeteryId";

            public const string AccountId = "AccountId";
        }
        public static class URLS
        {
            public const string COMAPNY_IMAGE_PATH = "http://admin.hatiaarms.com/Upload/";
           // public const string COMAPNY_IMAGE_PATH = "http://localhost:2495/Upload";
        }
            //for tablenames as in DB for Tracking
            public static class Tables
        {
            public const string Account = "Account";

            public const string AccountRole = "AccountRole";

            public const string Block = "Block";

            public const string Cemetery = "Cemetery";

            public const string Client = "Client";

            public const string Contact = "Contact";

            public const string ContactNiche = "ContactNiche";

            public const string ContactType = "ContactType";

            public const string Crypt = "Crypt";

            public const string ExceptionLog = "ExceptionLog";

            public const string Niche = "Niche";

            public const string NicheRole = "NicheRole";

            public const string Occupant = "Occupant";

            public const string OccupantNiche = "OccupantNiche";

            public const string Role = "Role";

            public const string TrackingLog = "TrackingLog";
        }

        [Serializable]
        public enum NicheRole
        {
            //[Description("NotUsed")]
            //NotUsed0 = 0,

            //[Description("NotUsed")]
            //NotUsed1 = 1,

            [Description("Companion")]
            Companion = 2,

            [Description("Linked")]
            Linked = 3
        }


     
    }
}