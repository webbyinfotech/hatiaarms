﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Tools
{
    public class Converstions
    {
        public static List<int> SelectionListToInt(List<string> list)
        {
            return list.Select(int.Parse).ToList();
        }
    }
}
