﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Tools
{
    public class CommonList
    {
        public class View
        {
            public string StateAbbr { get; set; }
            public string StateName { get; set; }
        }

        public static List<View> GetList()
        {
            var list = new List<View>();

            list.Add(new View() { StateAbbr = "AL", StateName = "Alabama" });
            list.Add(new View() { StateAbbr = "AK", StateName = "Alaska" });
            list.Add(new View() { StateAbbr = "AZ", StateName = "Arizona" });
            list.Add(new View() { StateAbbr = "AR", StateName = "Arkansas" });
            list.Add(new View() { StateAbbr = "CA", StateName = "California" });
            list.Add(new View() { StateAbbr = "CO", StateName = "Colorado" });
            list.Add(new View() { StateAbbr = "CT", StateName = "Connecticut" });
            list.Add(new View() { StateAbbr = "DC", StateName = "District of Columbia" });
            list.Add(new View() { StateAbbr = "DE", StateName = "Delaware" });
            list.Add(new View() { StateAbbr = "FL", StateName = "Florida" });
            list.Add(new View() { StateAbbr = "GA", StateName = "Georgia" });
            list.Add(new View() { StateAbbr = "HI", StateName = "Hawaii" });
            list.Add(new View() { StateAbbr = "ID", StateName = "Idaho" });
            list.Add(new View() { StateAbbr = "IL", StateName = "Illinois" });
            list.Add(new View() { StateAbbr = "IN", StateName = "Indiana" });
            list.Add(new View() { StateAbbr = "IA", StateName = "Iowa" });
            list.Add(new View() { StateAbbr = "KS", StateName = "Kansas" });
            list.Add(new View() { StateAbbr = "KY", StateName = "Kentucky" });
            list.Add(new View() { StateAbbr = "LA", StateName = "Louisiana" });
            list.Add(new View() { StateAbbr = "ME", StateName = "Maine" });
            list.Add(new View() { StateAbbr = "MD", StateName = "Maryland" });
            list.Add(new View() { StateAbbr = "MA", StateName = "Massachusetts" });
            list.Add(new View() { StateAbbr = "MI", StateName = "Michigan" });
            list.Add(new View() { StateAbbr = "MN", StateName = "Minnesota" });
            list.Add(new View() { StateAbbr = "MS", StateName = "Mississippi" });
            list.Add(new View() { StateAbbr = "MO", StateName = "Missouri" });
            list.Add(new View() { StateAbbr = "MT", StateName = "Montana" });
            list.Add(new View() { StateAbbr = "NE", StateName = "Nebraska" });
            list.Add(new View() { StateAbbr = "NV", StateName = "Nevada" });
            list.Add(new View() { StateAbbr = "NH", StateName = "New Hampshire" });
            list.Add(new View() { StateAbbr = "NJ", StateName = "New Jersey" });
            list.Add(new View() { StateAbbr = "NM", StateName = "New Mexico" });
            list.Add(new View() { StateAbbr = "NY", StateName = "New York" });
            list.Add(new View() { StateAbbr = "NC", StateName = "North Carolina" });
            list.Add(new View() { StateAbbr = "ND", StateName = "North Dakota" });
            list.Add(new View() { StateAbbr = "OH", StateName = "Ohio" });
            list.Add(new View() { StateAbbr = "OK", StateName = "Oklahoma" });
            list.Add(new View() { StateAbbr = "OR", StateName = "Oregon" });
            list.Add(new View() { StateAbbr = "PA", StateName = "Pennsylvania" });
            list.Add(new View() { StateAbbr = "RI", StateName = "Rhode Island" });
            list.Add(new View() { StateAbbr = "SC", StateName = "South Carolina" });
            list.Add(new View() { StateAbbr = "SD", StateName = "South Dakota" });
            list.Add(new View() { StateAbbr = "TN", StateName = "Tennessee" });
            list.Add(new View() { StateAbbr = "TX", StateName = "Texas" });
            list.Add(new View() { StateAbbr = "UT", StateName = "Utah" });
            list.Add(new View() { StateAbbr = "VT", StateName = "Vermont" });
            list.Add(new View() { StateAbbr = "VA", StateName = "Virginia" });
            list.Add(new View() { StateAbbr = "WA", StateName = "Washington" });
            list.Add(new View() { StateAbbr = "WV", StateName = "West Virginia" });
            list.Add(new View() { StateAbbr = "WI", StateName = "Wisconsin" });
            list.Add(new View() { StateAbbr = "WY", StateName = "Wyoming" });

            return list.OrderBy(c => c.StateAbbr).ToList();

        }

        public class Position
        {
            public string Value { get; set; }
            public string Display { get; set; }
        }

        public static List<Position> GetPositionList()
        {
            var list = new List<Position>();

            list.Add(new Position() { Value = "DB", Display = "DB" });
            list.Add(new Position() { Value = "DL", Display = "DL" });
            list.Add(new Position() { Value = "LB", Display = "LB" });
            list.Add(new Position() { Value = "QB", Display = "QB" });
            list.Add(new Position() { Value = "RB", Display = "RB" });
            list.Add(new Position() { Value = "WR", Display = "WR" });
            list.Add(new Position() { Value = "OL", Display = "OL" });
            list.Add(new Position() { Value = "K", Display = "K" });
            list.Add(new Position() { Value = "TE", Display = "TE" });
            return list;

        }
    }
}
