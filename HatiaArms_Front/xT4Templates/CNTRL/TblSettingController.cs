using System;
using System.Web.Mvc;
using BLL.ModelViews;
using Newtonsoft.Json;

namespace Web.Controllers
{
    public class TblSettingController : Controller
    {

        public ActionResult Index()
        {
            var data = BLL.Queries.TblSetting.GetAll();
            return View(data);
        }

        public ActionResult Details(Guid guid)
        {
            var data = BLL.Queries.TblSetting.GetByGuid(guid);
            return View(data);
        }

        public ActionResult Create()
        {
            var data = new BLL.ModelViews.TblSetting.Extend();
            return View("Details", data);
        }

        [HttpPost]
        public string Post(BLL.ModelViews.TblSetting.Extend collection)
        {
            var results = new JsonMessageView();

            var error = BLL.Validations.TblSetting.Validate(collection);
            if (error.Count > 0)
            {
                results.success = false;
                results.message = "Validation Failed";
                results.validation = error;
                return JsonConvert.SerializeObject(results);
            }


            if (collection.Guid == Guid.Empty)
            {
                // Create record
                var data = BLL.Commands.TblSetting.Create(collection);
                if (data.Guid  != Guid.Empty)
                {
                    results.success = true;
                    results.message = "Success creating " + collection.Title;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error creating " + collection.Title;
                }

            }
            else
            {
                // Update record
                var data = BLL.Commands.TblSetting.Update(collection);
                if (data != null)
                {
                    results.success = true;
                    results.message = "Success updating " + collection.Title;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error updating " + collection.Title;
                }
            }


            return JsonConvert.SerializeObject(results);
        }

         

        [HttpPost]
        public string Delete(Guid guid)
        {
            var results = new BLL.ModelViews.JsonMessageView();

            var isDeleted = BLL.Commands.TblSetting.Delete(guid);
            if (isDeleted)
            {
                results.success = true;
                results.message = "'Delete";
            }

            return JsonConvert.SerializeObject(results);
        }
    }
}
