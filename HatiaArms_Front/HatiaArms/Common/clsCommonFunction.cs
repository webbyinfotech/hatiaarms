﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Net.Mail;
using System.Configuration;

namespace HatiaArms.Common
{
    public static class clsCommonFunction
    {
        public static bool sendMailtoBulkUser(string email, string username, string message)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(ConfigurationManager.AppSettings["smtpUser"]);
                mail.From = new MailAddress(email);
                mail.Subject = "Test Mail ";
                string Body = "Hi " + username + ",<br/> Your Email ID :"+ email+ " <br> Your Message Has been Sended  : <br> " + message;
                Body = Body + "<br>Thank You..!!! <br>";
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["smtpServer"];
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential
                (ConfigurationManager.AppSettings["smtpUser"], ConfigurationManager.AppSettings["smtpPass"]);// Enter seders User name and password
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                smtp.Send(mail);
                //  Session["CoachRegiSuccssess"] = "true";
                return true;
            }

            catch (Exception ex)
            { return false;  }

        }
    }
}

