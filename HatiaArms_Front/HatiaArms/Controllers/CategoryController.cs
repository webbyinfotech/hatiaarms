﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HatiaArms.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Product(string id)
        {

            // get category id from its name
            var categoryData = BLL.Queries.TblCategory.GetById(id);
            int categoryID = 0;
            if (categoryData!=null)
            {
                categoryID = categoryData.ID;
            }
           
            // get product by category id
            var productData = BLL.Queries.TblProduct.GetByCategoryID(categoryID);
            ViewBag.CategoryName = id;
            ViewBag.ProductData = productData;
            return View();
        }

        public ActionResult ProductSearch(string Search)
        {
            var productData = BLL.Queries.TblProduct.GetBySearch(Search);
            ViewBag.ProductData = productData;
            return View("Product");
        }

        public ActionResult ProductDetails(int id)
        {
            var productData = BLL.Queries.TblProduct.GetById(id);
            try
            {
                productData.ImageList = new List<string>();
                var images = productData.Images.Split(',');
                foreach (var item in images)
                {
                    string img = BLL.Tools.Constants.URLS.COMAPNY_IMAGE_PATH + "/Product/Original/" + item;
                    productData.ImageList.Add(img);
                }
            }
            catch (Exception ex)
            { }
            ViewBag.ProductData = productData;
            return View();
        }

        public ActionResult StoreRatting(int pid,float rate)
        {
            String ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = Request.ServerVariables["REMOTE_ADDR"];
            }
            try
            {
                var productData = BLL.Queries.TblReview.GetByIPAddress(ip,pid);
                if(productData == null)
                {
                    BLL.Commands.TblReview.Create(new BLL.ModelViews.TblReview.Extend { UserName = ip,Product_Id = pid,Rating = rate});
                }
                else
                {
                    /*productData.Rating = rate;
                    BLL.Commands.TblReview.Update(productData);*/
                    return Json("false", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            { }
            //ViewBag.ProductData = productData;
            return Json("OK", JsonRequestBehavior.AllowGet);
        }



        public ActionResult ViewProduct(int id)
        {
            BLL.ModelViews.TblProduct.Extend product = BLL.Queries.TblProduct.GetById(id);
            try
            {
                product.ImageList = new List<string>();
                var images = product.Images.Split(',');
                foreach (var item in images)
                {
                    string img = BLL.Tools.Constants.URLS.COMAPNY_IMAGE_PATH + "/Product/Original/" + item;
                    product.ImageList.Add(img);
                }
            }
            catch(Exception ex)
            { }
            return Json( product,JsonRequestBehavior.AllowGet);
        }

    }
}