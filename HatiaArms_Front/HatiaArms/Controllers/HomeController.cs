﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HatiaArms.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Slider()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendMail(FormCollection collection)
        {
           if( Common.clsCommonFunction.sendMailtoBulkUser(collection["email"], collection["name"], collection["message"]))
                return Json("Done", JsonRequestBehavior.AllowGet);
           else
                return Json("Fail", JsonRequestBehavior.AllowGet);
        }

        public ActionResult SiteRate(double rate)
        {
            HttpCookie myCookie = new HttpCookie("ck_SiteRate");

            String ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = Request.ServerVariables["REMOTE_ADDR"];
            }
            try
            {

                var siteData = BLL.Queries.TblSiteReview.GetByIPAddress(ip);
                if (siteData == null)
                {
                    BLL.Commands.TblSiteReview.Create(new BLL.ModelViews.TblSiteReview.Extend { IP_Address = ip, Rating = rate });
                }
                else
                {

                    myCookie.Value = rate.ToString();
                    Response.Cookies.Add(myCookie);
                    return Json("false", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            { }

            myCookie.Value = rate.ToString();
            Response.Cookies.Add(myCookie);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
    }
}