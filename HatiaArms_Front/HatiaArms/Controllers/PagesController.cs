﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HatiaArms.Controllers
{
    public class PagesController : Controller
    {
        // GET: Pages
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Page(string id)
        {
            BLL.ModelViews.TblPageMaster.Extend PageMaster = BLL.Queries.TblPageMaster.GetByPageName(id);
            return View(PageMaster);
        }

        
    }
}