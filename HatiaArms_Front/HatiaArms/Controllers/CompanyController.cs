﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HatiaArms.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Product(int id)
        {
            var companyData = BLL.Queries.TblCompany.GetById(id);
            int companyID = 0;
            if (companyData != null)
            {
                companyID = companyData.ID;
            }

            // get product by category id
            var productData = BLL.Queries.TblProduct.GetByCompanyId(companyID);
            ViewBag.CompanyData = companyData;
            ViewBag.ProductData = productData;
            return View();
        }
    }
}