﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HatiaArms.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [RedirectingAction]
        public ActionResult Index()
        {
            return View();
        }
    }
}