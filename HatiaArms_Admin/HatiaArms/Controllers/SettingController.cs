﻿using BLL.ModelViews;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HatiaArms.Controllers
{
    public class SettingController : Controller
    {
        // GET: Setting
        /*public ActionResult Index()
        {
            return View();
        }*/
        public ActionResult Index()
        {
            var data = BLL.Queries.TblSetting.GetById(1);
            return View(data);
        }

        [HttpPost]
        public string Post(BLL.ModelViews.TblSetting.Extend collection)
        {
            var results = new JsonMessageView();

            var error = BLL.Validations.TblSetting.Validate(collection);
            if (error.Count > 0)
            {
                results.success = false;
                results.message = "Validation Failed";
                results.validation = error;
                return JsonConvert.SerializeObject(results);
            }

                // Update record
                var data = BLL.Commands.TblSetting.Update(collection);
                if (data != null)
                {
                    results.success = true;
                    results.message = "Success updating " ;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error updating ";
                }
            
            return JsonConvert.SerializeObject(results);
        }



        [HttpPost]
        public string Delete(int id)
        {
            var results = new BLL.ModelViews.JsonMessageView();

            var isDeleted = BLL.Commands.TblSetting.Delete(id);
            if (isDeleted)
            {
                results.success = true;
                results.message = "'Delete";
            }

            return JsonConvert.SerializeObject(results);
        }
    }
}