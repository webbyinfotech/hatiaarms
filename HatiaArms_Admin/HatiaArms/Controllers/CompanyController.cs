﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HatiaArms.Controllers
{
    public class CompanyController : Controller
    {
        public ActionResult Index()
        {
            var data = BLL.Queries.TblCompany.GetAll();
            return View(data);
        }
        public ActionResult Details(Guid guid)
        {
            try
            {
                ViewBag.PageName = "Company";
                var data = BLL.Queries.TblCompany.GetByGuid(guid);
                return View(data);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult Create()
        {
            try
            {
                ViewBag.PageName = "Gateways";
                var data = new BLL.ModelViews.TblCompany.Extend();
                data.Guid = Guid.Empty;
                return View("Details", data);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Post(BLL.ModelViews.TblCompany.Extend collection, HttpPostedFileBase ImageFile)
        {
            var results = new BLL.ModelViews.JsonMessageView();
            string image1 = "";

            try
            {
                //   string serverPath = "";
                if (ImageFile != null)
                {
                    string path = Server.MapPath("~/Upload/CompanyLogo/Original/");

                    string[] file = ImageFile.FileName.Split('.');
                    string[] Extention = ImageFile.ContentType.Split('/');
                    image1 = file[0] + "_" + System.DateTime.Now.ToString("ddMMyyhhmmss") + "." + Extention[1];
                    ImageFile.SaveAs(path + image1);
                    var pic = System.Drawing.Image.FromFile(path + image1);
                    if (pic.Height > 960 || pic.Width > 960)
                    {
                        Common.Utilities.ResizeStream(960, path + image1, Server.MapPath("~/Upload/CompanyLogo/Thumbnail/" + image1));
                    }
                    else
                    {
                        ImageFile.SaveAs(Server.MapPath("~/Upload/CompanyLogo/Thumbnail/") + image1);
                    }
                    ImageFile.InputStream.Flush();
                    ImageFile.InputStream.Dispose();
                    Common.Utilities.ResizeStream(100, path + image1, Server.MapPath("~/Upload/CompanyLogo/Thumbnail/") + image1);//create thubnail image.
                    if (System.IO.File.Exists(path + collection.Image))
                    {
                        System.IO.File.Delete(path + collection.Image);
                    }
                    //serverPath = "~/upload/Advertisement/Thumbnail/" + image1;
                }
                else
                {
                    image1 = collection.Image;
                }

                collection.Image = image1;
            }
            catch (Exception ex)
            {
                //return false;
            }


            try
            {
                var error = BLL.Validations.TblCompany.Validate(collection);
                if (error.Count > 0)
                {
                    results.success = false;
                    results.message = "Validation Failed";
                    results.validation = error;
                    return View("Details", new { guid = collection.Guid});
                }

                if (collection.Guid == Guid.Empty)
                {
                    // Create record
                    var data = BLL.Commands.TblCompany.Create(collection);
                    if (data.Guid != Guid.Empty)
                    {
                        results.success = true;
                        results.message = "Success creating " + collection.CompanyName;
                    }
                    else
                    {
                        results.success = false;
                        results.message = "There was an error creating " + collection.CompanyName;
                    }
                }
                else
                {
                    // Update record
                    var data = BLL.Commands.TblCompany.Update(collection);
                    if (data != null)
                    {
                        results.success = true;
                        results.message = "Success updating " + collection.CompanyName;
                    }
                    else
                    {
                        results.success = false;
                        results.message = "There was an error updating " + collection.CompanyName;
                    }
                }
            }
            catch (Exception ex)
            {
                //Guid gid = new Common.clsCommonFunctions().LogManager(Request.FilePath, ex, "Post() Payment Method Create Or Update");
                Response.Redirect("Index" );
            }
            if (results.success)
                return RedirectToAction("Index");
            else
                return View("Details", new { guid = collection.Guid });
        }

       [HttpPost]
        public string Delete(Guid guid)
        {
            var results = new BLL.ModelViews.JsonMessageView();
            try
            {
                var isDeleted = BLL.Commands.TblCompany.Delete(guid);
                if (isDeleted)
                {
                    results.success = true;
                    results.message = "Delete";
                }

            }
            catch (Exception ex)
            {
                Response.Redirect("Index");
            }
            return JsonConvert.SerializeObject(results);
        }

    }
}