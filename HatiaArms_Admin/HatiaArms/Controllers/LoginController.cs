using System;
using System.Web.Mvc;
using BLL.ModelViews;
using Newtonsoft.Json;

namespace HatiaArms.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            //var data = BLL.Queries.TblLogin.GetAll();
            //return View(data);
            var data = new BLL.ModelViews.TblLogin.Extend();


            if (Session["adminModel"] != null)
                return RedirectToAction("Index", "Home");
            else
                return View("Index", data);
        }


        public ActionResult ChangePassword()
        {


            if (Session["adminModel"] == null)
                return RedirectToAction("Index", "Home");
            else
            {
                var data = new BLL.ModelViews.TblLogin.Extend();
                data.Username = Session["adminModel"].ToString();
                data.ID = Convert.ToInt16(Session["adminId"]);
                return View("ChangePassword", data);

            }
        }

        [HttpPost]
        public string ChangePasswordPost(BLL.ModelViews.TblLogin.Extend collection)
        {
            var results = new JsonMessageView();
            var query = BLL.Queries.TblLogin.GetById(Convert.ToInt16(Session["adminId"]));

            if (query.Username != collection.Password)
            {
                results.success = false;
                results.message = "Old Password is Incorrect";
            }

            else
            {
                var data = BLL.Commands.TblLogin.PasswordUpdate(collection);
                if (data != null)
                {
                    results.success = true;
                    results.message = "Success updating ";
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error updating Password ";
                }

            }
            return JsonConvert.SerializeObject(results);
        }

        public ActionResult Logout()
        {
            Session["adminModel"] = null;
            return View();
        }


        [HttpPost]
        public string Post(BLL.ModelViews.TblLogin.Extend collection)
        {
            var results = new JsonMessageView();

            var query = BLL.Queries.TblLogin.GetAll();
            Boolean l_blFound = false;

            foreach (var f_data in query)
            {
                if (f_data.Username == collection.Username && f_data.Password == collection.Password)
                {
                    Session["adminModel"] = f_data.Username;
                    Session["adminId"] = f_data.ID;
                    l_blFound = true;
                    break;

                }
            }

            if (l_blFound)
            {
                results.success = true;
                // RedirectToAction("Index", "Home");
                //results.success = true;
                results.message = "Success Login ";
            }
            else
            {
                results.success = false;
                results.message = "Fail to Login ";
            }


            return JsonConvert.SerializeObject(results);
        }
    }
}
