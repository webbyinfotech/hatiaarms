﻿using BLL.ModelViews;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HatiaArms.Controllers
{
    public class PageMasterController : Controller
    {
        public ActionResult Index()
        {
            var data = BLL.Queries.TblPageMaster.GetAll();
            return View(data);
        }

        public ActionResult Details(Guid guid)
        {
            var data = BLL.Queries.TblPageMaster.GetByGuid(guid);
            return View(data);
        }

        public ActionResult Create()
        {
            var data = new BLL.ModelViews.TblPageMaster.Extend();
            return View("Details", data);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Post(BLL.ModelViews.TblPageMaster.Extend collection)
        {
            var results = new JsonMessageView();

            var error = BLL.Validations.TblPageMaster.Validate(collection);
            if (error.Count > 0)
            {
                results.success = false;
                results.message = "Validation Failed";
                results.validation = error;
                //return JsonConvert.SerializeObject(results);
            }


            if (collection.Guid == null)
            {
                // Create record
                var data = BLL.Commands.TblPageMaster.Create(collection);
                if (data.Guid != Guid.Empty)
                {
                    results.success = true;
                    results.message = "Success creating " + collection.PageName;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error creating " + collection.PageName;
                }

            }
            else
            {
                // Update record
                var data = BLL.Commands.TblPageMaster.Update(collection);
                if (data != null)
                {
                    results.success = true;
                    results.message = "Success updating " + collection.PageName;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error updating " + collection.PageName;
                }
            }
            //return JsonConvert.SerializeObject(results);
            return RedirectToAction("Index");
            
        }
        [HttpPost]
        public string Delete(Guid guid)
        {
            var results = new BLL.ModelViews.JsonMessageView();

            var isDeleted = BLL.Commands.TblPageMaster.Delete(guid);
            if (isDeleted)
            {
                results.success = true;
                results.message = "'Delete";
            }

            return JsonConvert.SerializeObject(results);
        }
    }
}