﻿using BLL.ModelViews;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace HatiaArms.Controllers
{
    public class ProductController : Controller
    {

        public ActionResult Index()
        {
            var data = BLL.Queries.TblProduct.GetAll();
            return View(data);
        }

        public ActionResult Details(Guid guid)
        {
            var data = BLL.Queries.TblProduct.GetByGuid(guid);
            return View(data);
        }

        public ActionResult Create()
        {
            var data = new BLL.ModelViews.TblProduct.Extend();
            return View("Details", data);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Post(BLL.ModelViews.TblProduct.Extend collection)
        {
            var results = new JsonMessageView();
            string image1 = null;
            int i = 0;
            string strimgStore = string.Empty;
            try
            {
                if (Request.Files.Count != 0)
                {
                    foreach (string img in Request.Files)
                    {

                        if (Request.Files[img].ContentLength != 0)
                        {
                            string path = Server.MapPath("~/Upload/Product/Original/");
                            string[] file = Request.Files[img].FileName.Split('.');
                            string[] Extention = Request.Files[img].ContentType.Split('/');

                            image1 = file[0] + "_" + System.DateTime.Now.ToString("ddMMyyhhmmss") + "." + Extention[1];
                            Request.Files[img].SaveAs(path + image1);
                            var pic = System.Drawing.Image.FromFile(path + image1);
                            if (pic.Height > 960 || pic.Width > 960)
                            {
                                Common.Utilities.ResizeStream(960, path + image1, Server.MapPath("~/Upload/Product/Thumbnail/" + image1));
                            }
                            else
                            {
                                Request.Files[img].SaveAs(Server.MapPath("~/Upload/Product/Thumbnail/") + image1);
                            }

                            Request.Files[img].InputStream.Flush();
                            Request.Files[img].InputStream.Dispose();
                            Common.Utilities.ResizeStream(450, path + image1, Server.MapPath("~/Upload/Product/Thumbnail/") + image1);//create thubnail image.

                            if (i == 0)
                            {
                                strimgStore = image1;
                            }
                            else
                            {
                                strimgStore = strimgStore + "," + image1;
                            }
                            i = i + 1;

                        }
                    }
                    string strimg = collection.Images;
                    if (strimg != null)
                    {
                        string[] strarrimg = strimg.Split(',');
                        foreach (var delimg in strarrimg)
                        {
                            if (System.IO.File.Exists(Server.MapPath("~/Upload/Product/Original/") + delimg))
                            {
                                System.IO.File.Delete(Server.MapPath("~/Upload/Product/Original/") + delimg);
                            }
                        }
                    }
                }
                else
                {
                    strimgStore = collection.Images;
                }
                collection.Images = strimgStore;
            }
            catch (Exception ex)
            {
                //return false;
            }
            var error = BLL.Validations.TblProduct.Validate(collection);
            if (error.Count > 0)
            {
                results.success = false;
                results.message = "Validation Failed";
                results.validation = error; return View("Details", new { guid = collection.Guid });
            }

            if (collection.Guid == null)
            {
                // Create record
                var data = BLL.Commands.TblProduct.Create(collection);
                if (data.Guid != Guid.Empty)
                {
                    results.success = true;
                    results.message = "Success creating " + collection.Name;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error creating " + collection.Name;
                }

            }
            else
            {
                // Update record
                var data = BLL.Commands.TblProduct.Update(collection);
                if (data != null)
                {
                    results.success = true;
                    results.message = "Success updating " + collection.Name;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error updating " + collection.Name;
                }
            }
            if (results.success)
                return RedirectToAction("Index");
            else
                return View("Details", new { guid = collection.Guid });
        }



        [HttpPost]
        public string Delete(Guid guid)
        {
            var results = new BLL.ModelViews.JsonMessageView();

            var isDeleted = BLL.Commands.TblProduct.Delete(guid);
            if (isDeleted)
            {
                results.success = true;
                results.message = "'Delete";
            }

            return JsonConvert.SerializeObject(results);
        }

    }
}