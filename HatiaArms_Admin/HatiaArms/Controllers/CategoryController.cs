﻿using BLL.ModelViews;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HatiaArms.Controllers
{
    public class CategoryController : Controller
    {
        public ActionResult Index()
        {
            var data = BLL.Queries.TblCategory.GetAll();
            return View(data);
        }

        public ActionResult Details(Guid guid)
        {
            var data = BLL.Queries.TblCategory.GetByGuid(guid);
            return View(data);
        }

        public ActionResult Create()
        {
            var data = new BLL.ModelViews.TblCategory.Extend();
            return View("Details", data);
        }

        [HttpPost]
        public string Post(BLL.ModelViews.TblCategory.Extend collection)
        {
            var results = new JsonMessageView();

            var error = BLL.Validations.TblCategory.Validate(collection);
            if (error.Count > 0)
            {
                results.success = false;
                results.message = "Validation Failed";
                results.validation = error;
                return JsonConvert.SerializeObject(results);
            }


            if (collection.Guid == null)
            {
                // Create record
                var data = BLL.Commands.TblCategory.Create(collection);
                if (data.Guid != Guid.Empty)
                {
                    results.success = true;
                    results.message = "Success creating " + collection.CategoryName;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error creating " + collection.CategoryName;
                }

            }
            else
            {
                // Update record
                var data = BLL.Commands.TblCategory.Update(collection);
                if (data != null)
                {
                    results.success = true;
                    results.message = "Success updating " + collection.CategoryName;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error updating " + collection.CategoryName;
                }
            }


            return JsonConvert.SerializeObject(results);
        }



        [HttpPost]
        public string Delete(Guid guid)
        {
            var results = new BLL.ModelViews.JsonMessageView();

            var isDeleted = BLL.Commands.TblCategory.Delete(guid);
            if (isDeleted)
            {
                results.success = true;
                results.message = "'Delete";
            }

            return JsonConvert.SerializeObject(results);
        }
    }
}