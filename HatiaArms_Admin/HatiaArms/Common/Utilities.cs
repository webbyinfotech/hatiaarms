﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
namespace HatiaArms.Common
{
    public class Utilities
    {
        public static void ResizeStream(int imageSize, string filePath, string outputPath)
        {
            var image = Image.FromFile(filePath);
            

            int thumbnailSize = imageSize;
            int newWidth, newHeight;

            if (image.Width > image.Height)
            {
                newWidth = thumbnailSize;
                newHeight = image.Height * thumbnailSize / image.Width;
            }
            else
            {
                newWidth = image.Width * thumbnailSize / image.Height;
                newHeight = thumbnailSize;
            }

            var thumbnailBitmap = new Bitmap(newWidth, newHeight);
           // var bmp1 = new Bitmap(thumbnailBitmap);
           

            var thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

            var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbnailGraph.DrawImage(image, imageRectangle);
             GC.Collect(); 
            
            thumbnailBitmap.Save(outputPath,image.RawFormat);

           
            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
               image.Dispose();
           
        }


}
}

