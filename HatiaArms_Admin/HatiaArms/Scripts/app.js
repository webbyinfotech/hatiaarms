﻿//---------------------------------------
var oTable;
$(document).ready(function () {

    $('#title').focus();

    //---------------------------------------
    //--- jquery.dataTables ---
    /* oTable = $('table.dataTable').dataTable({ 
         "bPaginate": false,
         "bLengthChange": true,
         "bFilter": true,
         "bSort": true,
         "bSortCellsTop": true,
         "sDom": '<"top">t<"bottom"><"clear">',
         aoColumnDefs: [{aTargets: [-1], 
             bSortable: false
         }],
         "bDestroy": true
     });
     */


    $("thead input").keyup(function () {
        /* Filter on the column (the index) of this element */
        oTable.fnFilter(this.value, $(".dataTable-input input").index(this));
    });
    //---------------------------------------

    //---------------------------------------
    toastr.options = {
        "positionClass": "toast-top-center"
    };
});


// New method created for add or update data - vardhman 23-12-2015
function ProcessRefunRecord(objForm, formButton, controller) {

    var formData = $(objForm).serialize();
    var PreviousURL = $(objForm).find('#PreviousURL').val();
    var CurrentURL = $(objForm).find('#CurrentURL').val();

    SpinningIconOnForButton(formButton);

    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: "/" + controller + "/PostRefund",
        data: formData,
        success: function (data) {

            if (data.success) {
                toastr.success(data.message);
                if (CurrentURL == undefined) {
                   // WaitAndRedirect();
                }
                else {
                    WaitAndRedirect();
                }
            }
            else {
                SpinningIconOffForButton(formButton);
                toastr.error(data.message + '. Please try again!');
                ProcessServerValidation(objForm, data.validation);
                return false;
            }
        },
        error: function (data) {
            alert(data);
        }
    });
}
//-------------------------------------------------------------------------------------------------------------------------------------------

// New method created for add or update data - ghanshyam 2-10-2015
function AddUpdateRecord(objForm, formButton, controller) {

    var formData = $(objForm).serialize();
    var PreviousURL = $(objForm).find('#PreviousURL').val();
    var CurrentURL = $(objForm).find('#CurrentURL').val();

    SpinningIconOnForButton(formButton);

    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: "/" + controller + "/Post",
        data: formData,
        success: function (data) {

            if (data.success) {
                toastr.success(data.message);
                if (CurrentURL == undefined) {
                    WaitAndRedirect(PreviousURL);
                }
                else {
                    WaitAndRedirect(CurrentURL);
                }
            }
            else {
                SpinningIconOffForButton(formButton);
                toastr.error(data.message + '. Please try again!');
                ProcessServerValidation(objForm, data.validation);
                return false;
            }
        },
        error: function (data) {
            alert(data);
        }
    });
}

// Added by ghanshyam for reload location
function Reload() {
    location.reload(true);
}

// Added by ghanshyam for delete record
function DeleteRecord(controller, guid) {

    //$("#iTrash").addClass('danger');
    //$("#iTrash").addClass('fa-spinner fa-spin');

    var result = confirm("Delete this record?");

    if (result == true) {
        $.ajax({
            type: 'Post',
            url: "/" + controller + '/Delete/',
            data: { guid: guid },
            success: function () {
                toastr.success(controller + ' record deleted successfully!');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //$("#iTrash").removeClass('danger');
                //$("#iTrash").removeClass('fa-spinner fa-spin');
                toastr.error('There was an error deleting ' + controller + ' record. Please try again!');
            }
        });
        setTimeout(function () { Reload(); }, 4000);
    }
}

// Added by ghanshyam for assigning sites to support group
function AssignSiteToSupportGroup(objForm, formButton, controller) {

    var formData = $(objForm).serialize();
    var PreviousURL = $(objForm).find('#PreviousURL').val();
    var CurrentURL = $(objForm).find('#CurrentURL').val();

    SpinningIconOnForButton(formButton);

    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: "/" + controller + "/AssignSites",
        data: formData,
        success: function (data) {

            if (data.success) {
                toastr.success(data.message);
                if (CurrentURL == undefined) {
                    WaitAndRedirect(PreviousURL);
                }
                else {
                    WaitAndRedirect(CurrentURL);
                }
            }
            else {
                SpinningIconOffForButton(formButton);
                toastr.error(data.message + '. Please try again!');
                ProcessServerValidation(objForm, data.validation);
                return false;
            }
        },
        error: function (data) {
            alert(data);
        }
    });
}

//-------------------------------------------------------------------------------------------------------------------------------------------




function UpdateRecord(objForm, formButton, controller, baseUrl) {

    var postUrl = '/' + controller + '/Post';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#parentGuid').val();

    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "index/" + parentGuid;
    }

    SpinningIconOnForButton(formButton);

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                WaitAndRedirect("/" + controller + "/" + parentGuid + "");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "");
            }

        } else {
            SpinningIconOffForButton(formButton);
            toastr.error(objJson.message + '. Please try again!');
            ProcessServerValidation(objForm, objJson.validation);
            return false;
        };
    });
};


function UpdateRecordstats(objForm, controller, baseUrl, id) {
    var postUrl = '/' + controller + '/Post';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#' + id).val();
    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "index/" + parentGuid;
    }

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                //return true;
                WaitAndRedirect("/" + controller + "/" + parentGuid + "?st=");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "?st=");
            }

        } else {
            toastr.error(objJson.message + '. Please try again!');
            ProcessServerValidation(objForm, objJson.validation);
            return false;
        };
    });
};


function UpdateRecordChangePassword(objForm, formButton, controller, baseUrl) {
    var postUrl = '/' + controller + '/ChangePasswordPost';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#parentGuid').val();
    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "index/" + parentGuid;
    }
    SpinningIconOnForButton(formButton);

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                WaitAndRedirect("/" + controller + "/" + parentGuid + "?st=");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "?st=");
            }

        } else {
            SpinningIconOffForButton(formButton);
            toastr.error(objJson.message + '. Please try again!');
            ProcessServerValidation(objForm, objJson.validation);
            return false;
        };
    });
};


function ProcessServerValidation(objForm, data) {
    var validator = $(objForm).validate();
    $.each(data, function (i, vObj) {
        var obj = {};
        obj[vObj.ValidationField] = vObj.ValidationMessage;

        validator.showErrors(obj);
    });
}

function DeleteRecordRemoveTr(element, controller) {
    var trDelete = $(element).closest('tr');

    // Spinning Icon Setup ------------
    var trIcon = $(element).children('i');
    //var trIconClasses = $(trIcon).attr('class');
    SpinningIconOnForRemoveTr(trIcon, trDelete);
    // ---------------------------------

    var guid = $(trDelete).data('id');
    var postUrl = '/' + controller + '/Delete/' + guid;

    var result = confirm("Delete this record?");
    if (result == true) {
        $.post(postUrl, null, function (data) {
            var objJson = jQuery.parseJSON(data);

            if (objJson.success) {
                toastr.success(controller + ' record deleted successfully!');
                RemoveTr(trDelete);
            } else {
                SpinningIconOffForRemoveTr(trIcon, trDelete);
                toastr.error('There was an error deleting ' + controller + ' record. Please try again!');
            };

        });
    } else {
        SpinningIconOffForRemoveTr(trIcon, trDelete);
    };
}

function DeleteRecordRemoveTrstats(element, controller, method) {
    var trDelete = $(element).closest('tr');

    // Spinning Icon Setup ------------
    var trIcon = $(element).children('i');
    //var trIconClasses = $(trIcon).attr('class');
    SpinningIconOnForRemoveTr(trIcon, trDelete);
    // ---------------------------------

    var guid = $(trDelete).data('id');
    var postUrl = '/' + controller + '/' + method + '/' + guid;

    var result = confirm("Delete this record?");
    if (result == true) {
        $.post(postUrl, null, function (data) {
            var objJson = jQuery.parseJSON(data);

            if (objJson.success) {
                toastr.success(controller + ' record deleted successfully!');
                RemoveTr(trDelete);
            } else {
                SpinningIconOffForRemoveTr(trIcon, trDelete);
                toastr.error('There was an error deleting ' + controller + ' record. Please try again!');
            };

        });
    } else {
        SpinningIconOffForRemoveTr(trIcon, trDelete);
    };
}


function RemoveTr(trDelete) {
    $(trDelete).removeClass().addClass('deleteTr');
    $(trDelete).find("td").removeClass().addClass('deleteTd');

    $(trDelete).fadeOut(1300, function () {
        $(trDelete).remove();
    });
}

function SpinningIconOnForRemoveTr(element, parentContainer) {
    $(parentContainer).addClass('danger');
    // $(element).removeClass();
    $(element).addClass('fa-spinner fa-spin');
}

function SpinningIconOffForRemoveTr(element, parentContainer) {
    $(parentContainer).removeClass('danger');
    $(element).removeClass('fa-spinner fa-spin');
    // $(element).addClass(ogClass);
}

function SpinningIconOnForButton(element) {
    $(element).attr('disabled', true);
    $(element).html(CreateSpinningIcon());
}

function SpinningIconOffForButton(element) {
    $(element).attr('disabled', false);
    $(element).html("Submit");
}

function CreateSpinningIcon() {
    var strHtml = "<i class='fa fa-spinner fa-spin'></i> Processing ... ";
    return strHtml;
}

function WaitAndRedirect(url) {
    setTimeout(function () {
        window.location = url;
    }, 1500);
}

function ListBoxMoveItems(selectListBox, targetListBox) {
    var selectedOpts = $(selectListBox).find(':selected');
    $(targetListBox).append($(selectedOpts).clone());
    $(selectedOpts).remove();
    // Reorder Listbox
    SortTexBoxList(selectListBox);
    SortTexBoxList(targetListBox);
}

function SortTexBoxList(texboxList) {
    var $r = $(texboxList).find('option');

    $r.sort(function (a, b) {
        if (a.text < b.text) return -1;
        if (a.text == b.text) return 0;
        return 1;
    });

    $($r).remove();

    $(texboxList).append($($r));
}

function UpdateDraggableSortNicheLayout(nicheItems) {
    var postUrl = "/Niche/UpdateDraggableSortNicheLayout";
    var formData = { itemsList: nicheItems };

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            // alert(objJson.success);
        } else {
            alert("There was an error .. try again");
        }
    });
}
// ------------------------------------------------------------
// ------------------------------------------------------------



