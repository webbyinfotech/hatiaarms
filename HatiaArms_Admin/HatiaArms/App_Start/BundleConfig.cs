﻿using System.Web;
using System.Web.Optimization;

namespace HatiaArms
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content").Include(
                "~/Content/style.css",
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-theme.css",
                "~/Content/datatable/jquery.dataTables.css",
                 "~/Content/datatable/dataTables.bootstrap.min.css",
                "~/Content/toastr.css"
                ));

            bundles.Add(new ScriptBundle("~/Scripts").Include(
                "~/Scripts/jquery-2.1.4.js",
                "~/Scripts/jquery-1.11.3.min.js",
                 // "~/Scripts/bootstrap.min.js",
                 "~/Scripts/jquery-2.1.4.intellisense.js",
                "~/Scripts/moment.js",
                "~/Scripts/toastr.js"
                ));

            bundles.Add(new ScriptBundle("~/Scripts/app").Include(
                "~/Scripts/datatable/jquery.validate.js",
                "~/Scripts/datatable/jquery-ui.custom.js",
                "~/Scripts/datatable/jquery.dataTables.js",
                "~/Scripts/app.js",
                "~/Scripts/datatable/jquery.js",
                "~/Scripts/datatable/dataTables.bootstrap.min.js"
                ));


            BundleTable.EnableOptimizations = false;
        }
    }
}