//------------------------------------------------------------------------------
// <auto-generated> This code was generated from a T4 template on:
//      2016-02-05 11:40:09 AM 
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BLL;
using BLL.ModelViews;
using BLL.Commands;
using BLL.Queries;
using BLL.Mappers;
using BLL.Validations;


namespace BLL.ModelViews {
    
    public class ValidationView {
    
        public string ValidationField { get; set; }
        public string ValidationMessage { get; set; }
    
    }
    
    public class JsonMessageView
        {
            private bool _success;
            public bool success {
                get
                {
                    return _success;
                }
                set { _success = value; }
            }
    
            private string _message;
            public string message {
                get
                {
                    return string.IsNullOrEmpty(_message) ? "base error" : _message;
                }
                set { _message = value; }
            }

            private string _status;
            public string status
            {
                get
                {
                    return string.IsNullOrEmpty(_status) ? " " : _status;
                }
                set
                {
                    _status = value;
                }
            }

            private string _statuscode;
            public string statuscode
            {
                get
                {
                    return string.IsNullOrEmpty(_statuscode) ? " " : _statuscode;
                }
                set
                {
                    _statuscode = value;
                }
            }
    
            public List<ValidationView> validation { get; set; }
        }
    
}
