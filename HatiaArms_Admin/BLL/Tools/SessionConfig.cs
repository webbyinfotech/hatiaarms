﻿namespace BLL.Tools
{
    using System.Web;

    public class SessionConfig
    {
        private static int accoutId;

        public static bool IsAuth
        {
            get
            {
                bool isAuth = false;

                if (HttpContext.Current.Session[Constants.Session.IsAuth] != null) isAuth = (bool)HttpContext.Current.Session[Constants.Session.IsAuth];

                return isAuth;
            }
            set
            {
                HttpContext.Current.Session[Constants.Session.IsAuth] = value;
            }
        }

        public static int ClientId
        {
            get
            {
                if (HttpContext.Current.Session == null) return 0;
                else
                {
                    int clientID = 0;
                    if (HttpContext.Current.Session[Constants.Session.ClientId] != null) clientID = (int)HttpContext.Current.Session[Constants.Session.ClientId];


                    return clientID;
                }
            }
            set
            {
                HttpContext.Current.Session[Constants.Session.ClientId] = value;
            }
        }

        public static int CemeteryId
        {
            get
            {
                if (HttpContext.Current.Session == null) return 0;
                else
                {
                    int cemeteryId = 0;
                    if (HttpContext.Current.Session[Constants.Session.CemeteryId] != null) cemeteryId = (int)HttpContext.Current.Session[Constants.Session.CemeteryId];


                    return cemeteryId;
                }
            }
            set
            {
                HttpContext.Current.Session[Constants.Session.CemeteryId] = value;
            }
        }

        public static int AccoutId
        {
            get
            {
                if (HttpContext.Current.Session == null) return 0;
                else
                {
                    int accountId = 0;
                    if (HttpContext.Current.Session[Constants.Session.AccountId] != null) accountId = (int)HttpContext.Current.Session[Constants.Session.AccountId];


                    return accountId;
                }
            }

            set
            {
                HttpContext.Current.Session[Constants.Session.AccountId] = value;
            }
        }
    }
}