
    using System;
    using System.ComponentModel;

    //for tablenames as in DB for Tracking
    public static class Tables
    {
    public const string TblCategory = "TblCategory";
    public const string TblCompany = "TblCompany";
    public const string TblLogin = "TblLogin";
    public const string TblPageMaster = "TblPageMaster";
    public const string TblProduct = "TblProduct";
    public const string TblReview = "TblReview";
    public const string TblSetting = "TblSetting";
    }


    [Serializable]
    public enum ErrorState
    {
        [Description("Critical")]
        Critical = 1,
        [Description("Significant")]
        Significant = 2,
        [Description("Normal")]
        Normal = 3,
        [Description("Gloabl - Application Level")]
        Global = 4,
        [Description("Admin Error")]
        Admin = 5
    }

    [Serializable]
    public enum TrackingState
    {
        [Description("Created")]
        Created = 1,
        [Description("Updated")]
        Updated = 2,
        [Description("Deleted")]
        Deleted = 3,
        [Description("AuthSuccess")]
        AuthSuccess = 4,
        [Description("AuthAttempt")]
        AuthAttempt = 5,
        [Description("WarningMSG")]
        Warning = 6,
    }

